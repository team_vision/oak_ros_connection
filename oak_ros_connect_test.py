import rclpy
from depthai_ros_msgs.msg import SpatialDetectionArray
from rclpy.node import Node
label_map = (
    "person",        "bicycle",      "car",           "motorbike",     "aeroplane",   "bus",         "train",       "truck",        "boat",
    "traffic light", "fire hydrant", "stop sign",     "parking meter", "bench",       "bird",        "cat",         "dog",          "horse",
    "sheep",         "cow",          "elephant",      "bear",          "zebra",       "giraffe",     "backpack",    "umbrella",     "handbag",
    "tie",           "suitcase",     "frisbee",       "skis",          "snowboard",   "sports ball", "kite",        "baseball bat", "baseball glove",
    "skateboard",    "surfboard",    "tennis racket", "bottle",        "wine glass",  "cup",         "fork",        "knife",        "spoon",
    "bowl",          "banana",       "apple",         "sandwich",      "orange",      "broccoli",    "carrot",      "hot dog",      "pizza",
    "donut",         "cake",         "chair",         "sofa",          "pottedplant", "bed",         "diningtable", "toilet",       "tvmonitor",
    "laptop",        "mouse",        "remote",        "keyboard",      "cell phone",  "microwave",   "oven",        "toaster",      "sink",
    "refrigerator",  "book",         "clock",         "vase",          "scissors",    "teddy bear",  "hair drier",  "toothbrush")
class OakTopicSubscriberNode(Node):
    subscribe_count = 0
    def __init__(self):
        super().__init__("oak_topic_subscriber_node")

        self.subscription_detect = self.create_subscription(
                SpatialDetectionArray,'/color/yolov4_Spatial_detections',self.on_subscribe_detect,10
        )
        self.get_logger().info("コンストラクタが呼ばれました")

    def on_subscribe_detect(self,msg):
        self.get_logger().info("count "+str(self.countup())+"\n")

        self.get_logger().info("subscribe!\n")
        for detect_pos in msg.detections:
            self.get_logger().info("subscribe : " + label_map[int(detect_pos.results[0].class_id)] + "\n" +str(detect_pos.bbox)+ "\n" + str(detect_pos.position) + "\n" )
    @classmethod
    def countup(cls):
        cls.subscribe_count += 1
        return cls.subscribe_count

def main(args = None):
    rclpy.init(args = args)
    node = OakTopicSubscriberNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == "__main__":
    main()
